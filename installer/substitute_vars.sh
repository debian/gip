#!/bin/sh
## This script takes a filename and a list of key/value pairs, and
## substitutes the keys in the given file by the value.
##

substitute() {
  FILE=$1
  shift
  MACROS="$@"
  OLDIFS=$IFS
  IFS=':'
  CMD=
  for LINE in $MACROS; do
    KEY=`echo $LINE | cut -d'=' -f1`
    VALUE=`echo $LINE | cut -d'=' -f2-`
    CMD="${CMD}s|%$KEY%|$VALUE|g;"
  done
  IFS=$OLDIFS
  sed "$CMD" < $FILE
  return 0
}

substitute $@

exit 0
