#!/bin/sh
## This script creates all the directories given at the commandline.
## Returns 0 on success, 1 otherwise.
##

for DIR in "$@"; do
  if [ ! -e "$DIR" ]; then
    echo "Creating directory $DIR... "
    mkdir -p $DIR
    if [ "$?" -ne "0" ]; then
      echo "Failed to create $DIR."
      exit 1
    fi
  fi
done

exit 0
