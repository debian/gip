#!/bin/sh
## Check whether the file's object file is up-to-date.
## Args: $1: The .c or .cc file to be checked.
##       $2: The corresponding object file.
## Returns -1 on an error, 0 if the file is up-to-date, 1 otherwise.
##

file_is_up2date() {
  FILE="$1"
  OBJECTFILE=`echo $1 | sed 's/\.c*/\.o/'`  # The corresponding objectfile.
  
  # If the file or the objectfile does not exist, we do not need to check.
  if [ ! -e "$FILE" -o ! -e "$OBJECTFILE" ]; then
    return 1
  fi
  # If the file is newer than the objectfile, we are already done.
  if [ "$FILE" -nt "$OBJECTFILE" ]; then
    return 1
  fi
  
  # Ending up here, we need to check all the dependencies.
  # Grabs a list of all dependencies (recursive).
  DEPENDS=`get_includes.sh $FILE`
  
  for HEADERFILE in $DEPENDS; do
    # Make sure that the headerfile exists.
    if [ ! -e "$HEADERFILE" ]; then
      echo "THIS IS LAME: $HEADERFILE"
      return 1
    fi
    # Check whether the file is newer than the object file.
    if [ "$HEADERFILE" -nt "$OBJECTFILE" ]; then
      return 1
    fi
  done
  
  echo Up-to-date check: $FILE is unchanged.
  return 0
}

file_is_up2date $1 || exit 1
exit 0
