#!/bin/sh
## Check whether all of the given libraries are installed.
## Returns 0 if all of the libs exist.
##
install_locales() {
  LOCALEFILES="$@"
  for FILE in $LOCALEFILES; do
    LOCALE=`echo $FILE | sed 's/.*\///; s/\.mo//'`
    mkdir -p $POTDIR/$LOCALE/LC_MESSAGES/
    cp $FILE $POTDIR/$LOCALE/LC_MESSAGES/$DOMAIN.mo || return 1
  done
  return 0
}


###############################################################################
# Use getopt to determine the allowed options.
###############################################################################
getopt -l "potdir:,domain:" -- "p:d:" $* 1>/dev/null
if [ $? != 0 ]; then
  print_usage
  exit 1
fi

for i in "$@"; do
  case $i in
    "-p" | "--potdir")
      POTDIR="$2"
      shift 2;;
    "-d" | "--domain")
      DOMAIN="$2"
      shift 2;;
    # End of options, only command arguments left.
    "--")
      shift
      break;;
  esac
done

if [ ! -d $POTDIR ]; then
  echo "No such directory: $POTDIR." >&2
  exit 1
fi

install_locales $@ || exit 1
exit 0
