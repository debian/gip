#!/bin/sh
## Check whether all of the given libraries are installed.
## Returns 0 if all of the libs exist.
##
check_libs() {
  LIBS="$@"
  ERROR=0
  for LIB in $LIBS; do
    pkg-config --exists $LIB
    if [ "$?" -ne "0" ]; then
      echo "Error: $LIB required but not installed." >&2
      echo "Please install the $LIB library's development package first." >&2
      ERROR=1
    else
      echo "Checking $LIB... installed!"
    fi
  done
  
  if [ "$ERROR" -eq 1 ]; then
    echo "$PACKAGENAME installation aborted." >&2
    return 1
  fi
  return 0
}

check_libs $@ || exit 1
exit 0
