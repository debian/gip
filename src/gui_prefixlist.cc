/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gui_prefixlist.h"

//#define _DEBUG_


/******************************************************************************
 * Constructor/Destructor
 ******************************************************************************/
GUIPrefixList::GUIPrefixList()
{
  // Attach the category liststore to the treeview.
  store = Gtk::ListStore::create(columns);
  set_model(store);
  get_selection()->set_mode(SELECTION_MULTIPLE);
  append_column(_("Prefix"),      columns.prefixes);
  append_column(_("Subnet Mask"), columns.subnets);
  set_rules_hint();
}


GUIPrefixList::~GUIPrefixList()
{
}


/******************************************************************************
 * Public
 ******************************************************************************/
/* Add one prefix to the list. */
void GUIPrefixList::insert(Prefix* prefix)
{
#ifdef _DEBUG_
  printf("GUIPrefixList::insert(*): Called.\n");
#endif
  char          net_txt[36];
  char          len_txt[4];
  char          mask_txt[36];
  unsigned long mask = 0;
  
  if (ipv4_integer2ip(prefix->net, net_txt) < 0)
    return;
  if (ipv4_pfxlen2mask(prefix->len, &mask) < 0)
    return;
  if (ipv4_integer2ip(mask, mask_txt) < 0)
    return;
  snprintf(len_txt, 3, "%i", prefix->len);
  
  string prefix_txt = net_txt;
  prefix_txt.append("/");
  prefix_txt.append(len_txt);
  if (prefixes.find(prefix_txt) != prefixes.end())
    return;
  prefixes[prefix_txt] = mask_txt;
  
  Gtk::TreeModel::Row row = *store->append();
  row[columns.prefixes] = prefix_txt;
  row[columns.subnets]  = mask_txt;
}


/* Add a list of prefixes to the list. */
void GUIPrefixList::insert(Prefix** prefixlist)
{
#ifdef _DEBUG_
  printf("GUIPrefixList::insert(**): Called.\n");
#endif
  int i = -1;
  while (prefixlist[++i])
    insert(prefixlist[i]);
}


/* Clear the list. */
void GUIPrefixList::clear(void)
{
  store->clear();
  prefixes.clear();
}


/* Returns the content of the prefixlist as one string. */
string GUIPrefixList::get_as_text(void)
{
  Glib::RefPtr<TreeSelection> selection = get_selection();
  list<TreePath>              selected  = selection->get_selected_rows();
  list<TreePath>::iterator    iter      = selected.begin();
  string content;
  while (iter != selected.end()) {
    TreeIter treeiter = store->get_iter(*iter);
    string line = treeiter->get_value(columns.prefixes)
                + " "
                + treeiter->get_value(columns.subnets)
                + "\n";
    content.append(line);
    iter++;
  }
  return content;
}


/******************************************************************************
 * Protected
 ******************************************************************************/
