/* The Cantus project.
 * (c)2002, 2003, 2004 by Samuel Abels (spam debain org)
 * This project's homepage is: http://www.debain.org/cantus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gui_aboutbox.h"


GUIAboutBox::GUIAboutBox(void)
{
  Label*  label  = NULL;
  VBox*   vbox   = get_vbox();
  Button* button = add_button(Stock::OK, 0);
  char version[2048];
  
  char* format = g_strdup_printf("<big>%s</big>", _("Gip Release %s %s"));
  snprintf(version, 2047, format, VERSION, SUBVERSION);
  g_free(format);
  
  label = new Label(version, 0.5, 0.5);
  label->set_use_markup(TRUE);
  label->set_justify(JUSTIFY_CENTER);
  label->set_line_wrap(TRUE);
  label->set_padding(10, 10);
  vbox->pack_start(*manage(label), FALSE, TRUE);
  
  string credits    = _("Translation of this version by:");
  string translator = _("translator_credits");
  if (translator == "translator_credits")
    credits = "";
  else
    credits.append(translator + ".");
  
  string text = _("Gip, the Internet Protocol Calculator for the GNOME desktop"
                  " environment was written and published under the terms"
                  " of the GPL (General Public License V2)\n"
                  "by Samuel Abels");
  text.append("\n\n");
  text.append(_("Thank You for choosing Gip!"));
  
  string copyright = "<small>";
  copyright.append(_("Copyright 2004. All rights reserved."));
  copyright.append("\nhttp://www.debain.org</small>");
  
  label = new Label(text, 0.5, 0.5);
  label->set_use_markup(TRUE);
  label->set_justify(JUSTIFY_CENTER);
  label->set_line_wrap(TRUE);
  label->set_padding(10, 10);
  vbox->pack_start(*label, FALSE, TRUE);
  
  label = new Label(credits, 0.5, 0.5);
  label->set_justify(JUSTIFY_CENTER);
  label->set_line_wrap(TRUE);
  label->set_padding(10, 10);
  vbox->pack_start(*manage(label), FALSE, TRUE);
  
  label = new Label(copyright, 0.5, 0.5);
  label->set_use_markup(TRUE);
  label->set_justify(JUSTIFY_CENTER);
  label->set_line_wrap(TRUE);
  label->set_padding(10, 10);
  vbox->pack_start(*manage(label), FALSE, TRUE);
  
  vbox->show_all();
  
  button->signal_clicked().connect(sigc::mem_fun(*this, &GUIAboutBox::hide));
}


GUIAboutBox::~GUIAboutBox(void)
{
}
