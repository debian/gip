/* lib_shellpattern.
 * (c)2002, 2003 by Samuel Abels (spam debain org)
 * This project's homepage is: http://www.debain.org/cantus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_LIB_SHELLPATTERN_H
#define HAVE_LIB_SHELLPATTERN_H

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#ifdef _CPLUSPLUS_
extern "C" {
#endif

/* Returns bool: Whether or not "string" matches the glob pattern "pattern". */
short int shellpattern_match(
                      const char *string,
                      const char *pattern,
                      short int   casess);

/* Returns bool: Whether or not "string" matches one of the glob patterns in
 * "pattern[]". */
short int shellpattern_match_any(
                      const char *string,
                      const char *pattern[],
                      short int   casess);

/* Returns bool and stores all substrings (=strings embraced by \( and \)
 * markers) in "match[]". */
short int shellpattern_match_grab(
                      const char *string,
                      const char *pattern,
                      short int   casess,
                      char      **match);

/* Returns bool: Whether or not "string" matches "regexp". */
short int regexp_match(
                      const char *string,
                      const char *regexp,
                      short int   casess);

/* This function will compare a regexp with a string and
 * return the content of all embraced matches, or NULL. Do not forget to free
 * the content of "matches[]"!
 */
short int regexp_match_grab(const char *string,
                            const char *regexp,
                            short int casess,
                            char **matches);

/* Frees the matches previously returned by regexp_match_grab.
 */
void free_regexp_matches(char **matches);

/* Returns bool: Whether or not the regexp can be compiled. */
short int regexp_is_valid(const char *regexp);

#ifdef _CPLUSPLUS_
}
#endif

#endif
