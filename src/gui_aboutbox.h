/* The Cantus project.
 * (c)2002, 2003, 2004 by Samuel Abels (spam debain org)
 * This project's homepage is: http://www.debain.org/cantus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_GUI_ABOUTBOX_H
#define HAVE_GUI_ABOUTBOX_H

#include <gtkmm.h>
#include <libintl.h>

#define _(String) gettext (String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop (String)

using namespace std;
using namespace Gtk;


class GUIAboutBox : public Dialog {
public:
  GUIAboutBox();
  ~GUIAboutBox();
  
protected:
  Image *image;
};

#endif
