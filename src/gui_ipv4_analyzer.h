/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_GUI_IPV4_ANALYZER_H
#define HAVE_GUI_IPV4_ANALYZER_H

#include <iostream>
#include <gtkmm.h>
#include <libintl.h>
#include <map>
#include "lib_ipv4.h"
#include <sigc++/sigc++.h>

#define _(String) gettext (String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop (String)

using namespace std;
using namespace Gtk;

class GUIIPv4Analyzer : public Table {
public:
  GUIIPv4Analyzer();
  ~GUIIPv4Analyzer();
  
  /* Triggered whenever the IP field has been changed. */
  sigc::signal2<void, unsigned long, unsigned long>      signal_ip_changed;
  /* Triggered whenever the netmask field has been changed. */
  sigc::signal2<void, unsigned long, unsigned long>      signal_mask_changed;
  /* Triggered whenever the prefixlength field has been changed. */
  sigc::signal2<void, unsigned long, unsigned short int> signal_pfxlen_changed;
  
  /* Switch event emissions off. */
  void lock_signals(void);
  
  /* Switch event emissions on (default). */
  void unlock_signals(void);
  
  /* Returns the value of the IP address field. */
  unsigned long get_ip(void);
  
  /* Sets the value of the IP address field. */
  void set_ip(unsigned long ip);
  
  /* Sets the value of the IP netmask field. */
  void set_mask(unsigned long mask);
  
  /* Sets the value of the prefixlength field. */
  void set_pfxlen(unsigned short int pfxlen);
  
  /* Sets the value of the "Output" -> "Range" fields. */
  void set_output_range(unsigned long from, unsigned long to);
  
  /* Sets the value of the "Output" -> "Number of Subnets" fields. */
  void set_output_subnets(unsigned int num);
  
  /* Sets the value of the "Output" -> "Number of Hosts" fields. */
  void set_output_hosts(unsigned int num);
  
  /* Sets the value of the "Output" -> "Number of Hosts, including Net/BrdCast"
   * field.
   */
  void set_output_hosts_all(unsigned int num);
  
  /* Sets the value of the "Output" -> "Network" field. */
  void set_output_network(unsigned long network);
  
  /* Sets the value of the "Output" -> "Broadcast Address" field. */
  void set_output_broadcast(unsigned long broadcast);
  
  /* Sets the value of the "Output (Binary)" -> "IP" field. */
  void set_output_bin_ip(unsigned long ip);
  
  /* Sets the value of the "Output (Binary)" -> "Netmask" field. */
  void set_output_bin_mask(unsigned long mask);
  
  /* Sets the value of the "Output (Binary)" -> "Network Address" field. */
  void set_output_bin_network(unsigned long net);
  
  /* Sets the value of the "Output (Binary)" -> "Broadcast Address" field. */
  void set_output_bin_broadcast(unsigned long broadcast);
  
  /* Sets the value of the "Output (Hexadecimal)" -> "IP" field. */
  void set_output_hex_ip(unsigned long ip);
  
  /* Sets the value of the "Output (Hexadecimal)" -> "Netmask" field. */
  void set_output_hex_mask(unsigned long mask);
  
  /* Sets the value of the "Output (Hexadecimal)" -> "Network Address" field. */
  void set_output_hex_network(unsigned long net);
  
  /* Sets the value of the "Output (Hexadecimal)" -> "Broadcast Address" field. */
  void set_output_hex_broadcast(unsigned long broadcast);
  
protected:
  int  map_ip_input(double* value, SpinButton* spin);
  bool map_ip_output(SpinButton* spin);
  int  map_mask_input(double* value, SpinButton* spin);
  bool map_mask_output(SpinButton* spin);
  void on_ip_changed(void);
  void on_mask_changed(void);
  void on_pfxlen_changed(void);
  
  bool lock_events;
  std::map<string, Widget*> widgets;  // Map Widgetname <-> Widget.
};

#endif
