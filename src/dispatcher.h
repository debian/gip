/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_DISPATCHER_H
#define HAVE_DISPATCHER_H

#include <sigc++/class_slot.h>
#include "gui_mainwindow.h"
#include "lib_ipv4.h"

class Dispatcher : public sigc::trackable {
public:
  Dispatcher();
  ~Dispatcher();
  
protected:
  void get_prefixes_from_range(unsigned long from, unsigned long to);
  void on_ipv4_analyzer_ip_changed(unsigned long ip, unsigned long mask);
  void on_ipv4_analyzer_mask_changed(unsigned long ip, unsigned long mask);
  void on_ipv4_analyzer_pfxlen_changed(unsigned long ip,
                                       unsigned short int pfxlen);
  void on_ipv4_subnet_calculator_range_changed(unsigned long from,
                                               unsigned long to);
  void on_ipv4_subnet_splitter_changed(unsigned long from,
                                       unsigned long to,
                                       unsigned short int pfxlen);
};

#endif
