/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_GUI_MAINWINDOW_H
#define HAVE_GUI_MAINWINDOW_H

#include <iostream>
#include <gtkmm.h>
#include <libintl.h>
#include <map>
#include "gui_ipv4_analyzer.h"
#include "gui_ipv4_subnet_calculator.h"
#include "gui_ipv4_subnet_splitter.h"
#include "gui_aboutbox.h"

#define _(String) gettext (String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop (String)

using namespace std;
using namespace Gtk;

class GUIMainwindow : public Window {
public:
  GUIMainwindow();
  ~GUIMainwindow();
  
  /* Switch event emissions off. */
  void lock_signals(void);
  
  /* Switch event emissions on (default). */
  void unlock_signals(void);
  
  GUIIPv4Analyzer         ipv4_analyzer;
  GUIIPv4SubnetCalculator ipv4_subnet_calculator;
  GUIIPv4SubnetSplitter   ipv4_subnet_splitter;
  
protected:
  GUIAboutBox aboutbox;
  MenuBar*    build_menu(void);
  void        on_file_quit_clicked(void);
  void        on_edit_cut_clicked(void);
  void        on_edit_copy_clicked(void);
  void        on_edit_paste_clicked(void);
  void        on_clipboard_text_received(const Glib::ustring& text);
  void        on_edit_delete_clicked(void);
  void        on_help_about_clicked(void);
};

#endif
