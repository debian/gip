/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifndef HAVE_GUI_PREFIXLIST_H
#define HAVE_GUI_PREFIXLIST_H

#include <iostream>
#include <gtkmm.h>
#include <libintl.h>
#include <map>
#include "lib_ipv4.h"

#define _(String) gettext (String)
#define gettext_noop(String) (String)
#define N_(String) gettext_noop (String)

using namespace std;
using namespace Gtk;

struct ltstr {
  bool operator()(const char* s1, const char* s2) const {
    return strcmp(s1, s2) < 0;
  }
};


class GUIPrefixList : public TreeView {
public:
  GUIPrefixList();
  ~GUIPrefixList();
  
  /* Add one prefix to the list. */
  void insert(Prefix* prefix);
  
  /* Add a list of prefixes to the list. */
  void insert(Prefix** prefixlist);
  
  /* Update the list. */
  void update(void);
  
  /* Clear the list. */
  void clear(void);
  
  /* Returns the content of the prefixlist in a list. */
  string get_as_text(void);
  
protected:
  // List model columns.
  class ModelColumns : public Gtk::TreeModel::ColumnRecord {
  public:
    Gtk::TreeModelColumn<Glib::ustring> prefixes;
    Gtk::TreeModelColumn<Glib::ustring> subnets;
    ModelColumns() {
      add(prefixes);
      add(subnets);
    }
  };
  Glib::RefPtr<Gtk::ListStore> store;
  ModelColumns                 columns;
  std::map<string, string>     prefixes;  // A list of all prefixes.
};

#endif
