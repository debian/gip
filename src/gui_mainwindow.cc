/*
 *  Author: Samuel Abels <spam debain org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gui_mainwindow.h"
#include <sigc++/sigc++.h>

//#define _DEBUG_


/******************************************************************************
 * Constructor/Destructor
 ******************************************************************************/
GUIMainwindow::GUIMainwindow()
{
#ifdef _DEBUG_
  printf("GUIMainwindow::GUIMainwindow(): Called.\n");
#endif
  VBox*     vbox     = new VBox;
  Notebook* notebook = new Notebook;
  notebook->append_page(ipv4_analyzer,
                        _("IPv4 Address Analyzer"));
  notebook->append_page(ipv4_subnet_calculator,
                        _("IPv4 Range to Prefix Converter"));
  notebook->append_page(ipv4_subnet_splitter,
                        _("IPv4 Subnet Calculator"));
  //notebook->set_border_width(12);
  vbox->pack_start(*manage(build_menu()), FALSE, FALSE);
  vbox->pack_start(*manage(notebook));
  vbox->show_all();
  add(*manage(vbox));
  set_title(_("Internet Protocol Calculator"));
}


GUIMainwindow::~GUIMainwindow()
{
#ifdef _DEBUG_
  printf("GUIMainwindow::~GUIMainwindow(): Called.\n");
#endif
}


/******************************************************************************
 * Public
 ******************************************************************************/
/* Switch event emissions off. */
void GUIMainwindow::lock_signals(void)
{
  ipv4_analyzer.lock_signals();
  ipv4_subnet_calculator.lock_signals();
  ipv4_subnet_splitter.lock_signals();
}


/* Switch event emissions on. */
void GUIMainwindow::unlock_signals(void)
{
  ipv4_analyzer.unlock_signals();
  ipv4_subnet_calculator.unlock_signals();
  ipv4_subnet_splitter.unlock_signals();
}


/******************************************************************************
 * Private
 ******************************************************************************/
MenuBar* GUIMainwindow::build_menu(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::build_form(): Called.\n");
#endif
  MenuBar* menubar = new MenuBar;
  Menu*    menu    = NULL;
  
  // Add the "File" menu.
  menu = new Menu;
  menubar->items().push_back(Menu_Helpers::MenuElem(_("_File"), *menu));
  menu->items().push_back(Menu_Helpers::StockMenuElem(Stock::QUIT,
                    sigc::mem_fun(*this, &GUIMainwindow::on_file_quit_clicked)));
  
  // Add the "Edit" menu.
  menu = new Menu;
  menubar->items().push_back(Menu_Helpers::MenuElem(_("_Edit"), *menu));
  menu->items().push_back(Menu_Helpers::StockMenuElem(Stock::CUT,
                    sigc::mem_fun(*this, &GUIMainwindow::on_edit_cut_clicked)));
  menu->items().push_back(Menu_Helpers::StockMenuElem(Stock::COPY,
                    sigc::mem_fun(*this, &GUIMainwindow::on_edit_copy_clicked)));
  menu->items().push_back(Menu_Helpers::StockMenuElem(Stock::PASTE,
                    sigc::mem_fun(*this, &GUIMainwindow::on_edit_paste_clicked)));
  menu->items().push_back(Menu_Helpers::StockMenuElem(Stock::DELETE,
                    sigc::mem_fun(*this, &GUIMainwindow::on_edit_delete_clicked)));
  
  // Add the "Help" menu.
  menu = new Menu;
  menubar->items().push_back(Menu_Helpers::MenuElem(_("_Help"), *menu));
  menu->items().push_back(Menu_Helpers::MenuElem(_("_About"),
                    sigc::mem_fun(*this, &GUIMainwindow::on_help_about_clicked)));
  
  return menubar;
}


void GUIMainwindow::on_file_quit_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_file_quit_clicked(): Called.\n");
#endif
  hide();
}


void GUIMainwindow::on_edit_cut_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_edit_cut_clicked(): Called.\n");
#endif
  on_edit_copy_clicked();
  on_edit_delete_clicked();
}


void GUIMainwindow::on_edit_copy_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_edit_copy_clicked(): Called.\n");
#endif
  Glib::RefPtr<Clipboard> clipboard = Clipboard::get();
  // Handle entry widgets.
  Entry* entry = dynamic_cast<Entry*>(get_focus());
  if (entry) {
    int start, end;
    entry->get_selection_bounds(start, end);
    clipboard->set_text(entry->get_text().substr(start, end - start));
    return;
  }
  
  // Handle treeview widgets.
  GUIPrefixList* pfxlist = dynamic_cast<GUIPrefixList*>(get_focus());
  if (pfxlist) {
    string text = pfxlist->get_as_text();
    clipboard->set_text(text);
    return;
  }
}


void GUIMainwindow::on_edit_paste_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_edit_paste_clicked(): Called.\n");
#endif
  Glib::RefPtr<Clipboard> clipboard = Clipboard::get();
  clipboard->request_text(
                sigc::mem_fun(*this, &GUIMainwindow::on_clipboard_text_received));
}


void GUIMainwindow::on_clipboard_text_received(const Glib::ustring& text)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_clipboard_text_received(): Called.\n");
#endif
  Entry* entry = dynamic_cast<Entry*>(get_focus());
  if (entry) {
    int start, end;
    entry->get_selection_bounds(start, end);
    entry->set_text(entry->get_text().replace(start, end - start, text));
    return;
  }
}


void GUIMainwindow::on_edit_delete_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_edit_delete_clicked(): Called.\n");
#endif
  Entry* entry = dynamic_cast<Entry*>(get_focus());
  if (entry) {
    int           start, end;
    entry->get_selection_bounds(start, end);
    entry->set_text(entry->get_text().erase(start, end - start));
    return;
  }
}


void GUIMainwindow::on_help_about_clicked(void)
{
#ifdef _DEBUG_
  printf("GUIMainwindow::on_help_about_clicked(): Called.\n");
#endif
  aboutbox.show();
}
