NAME=gip
VERSION=`grep '^VERSION=' build.sh | cut -d'=' -f2`
PACKAGE=$(NAME)-$(VERSION)
PREFIX=/usr/local
DISTDIR=/pub/code/releases/$(NAME)

###################################################################
# Project-specific targets.
###################################################################

###################################################################
# Standard targets.
###################################################################
clean:
	./clean.sh

dist-clean: clean
	rm -Rf $(PACKAGE)*

doc:
	# No docs yet.

install:
	./build.sh --install --prefix $(PREFIX)

uninstall:
	# No such target yet. Sorry.

tests:
	# No tests yet.

###################################################################
# Package builders.
###################################################################
targz:
	./build.sh --dist
	./clean.sh

tarbz:
	./build.sh --dist
	tar xzf $(PACKAGE).tar.gz
	tar cjf $(PACKAGE).tar.bz2 $(PACKAGE)
	./clean.sh

deb:
	# No debian package yet.

dist: targz tarbz deb

###################################################################
# Publishers.
###################################################################
dist-publish: dist
	mkdir -p $(DISTDIR)/
	mv $(PACKAGE).* $(DISTDIR)

doc-publish: doc
