# translation of gip-1.5.0.po to Danish
# Danish translation of Gip.
# Copyright (C) 2008 Gip development team.
# This file is distributed under the same license as the Gip package.
#
# Joe Hansen <joedalton2@yahoo.dk>, 2008.
# Keld Simonsen <keld@dkuug.dk>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: gip-1.5.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-05-20 20:08+0200\n"
"PO-Revision-Date: 2008-07-28 13:47+0200\n"
"Last-Translator: Keld Simonsen <keld@dkuug.dk>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: ../src/gui_aboutbox.cc:34
#, c-format
msgid "Gip Release %s %s"
msgstr "Gip version %s %s"

#: ../src/gui_aboutbox.cc:45
msgid "Translation of this version by:"
msgstr "OversÃŠttelse foretaget af:"

#: ../src/gui_aboutbox.cc:46
msgid "translator_credits"
msgstr "Joe Hansen"

#: ../src/gui_aboutbox.cc:52
msgid ""
"Gip, the Internet Protocol Calculator for the GNOME desktop environment was written and published under the terms of the GPL (General Public License V2)\n"
"by Samuel Abels"
msgstr ""
"Gip, internetprotokolberegner til GNOME-skrivebordsmiljÃžet, er programmeret og udgivet under betingelserne i GPL (General Public License V2)\n"
"af Samuel Abels"

#: ../src/gui_aboutbox.cc:57
msgid "Thank You for choosing Gip!"
msgstr "Tak fordi du valgte Gip!"

#: ../src/gui_aboutbox.cc:60
msgid "Copyright 2004. All rights reserved."
msgstr "Ophavsret 2004. Alle rettigheder forbeholdt."

# ordbog: input, inddata, indlÃŠsning
# ordliste: inddata
#: ../src/gui_ipv4_analyzer.cc:53
msgid "Input"
msgstr "Inddata"

#: ../src/gui_ipv4_analyzer.cc:62 ../src/gui_ipv4_analyzer.cc:204
#: ../src/gui_ipv4_analyzer.cc:254
msgid "IP address:"
msgstr "IP-adresse:"

#: ../src/gui_ipv4_analyzer.cc:65 ../src/gui_ipv4_analyzer.cc:207
#: ../src/gui_ipv4_analyzer.cc:257
msgid "Network mask:"
msgstr "NetvÃŠrksmaske:"

#: ../src/gui_ipv4_analyzer.cc:68
msgid "Prefix length:"
msgstr "PrÃŠfiks lÃŠngde:"

#: ../src/gui_ipv4_analyzer.cc:133
msgid "Output"
msgstr "Uddata"

#: ../src/gui_ipv4_analyzer.cc:143 ../src/gui_ipv4_subnet_calculator.cc:32
#: ../src/gui_ipv4_subnet_splitter.cc:32
msgid "Address range:"
msgstr "Adresseinterval:"

#: ../src/gui_ipv4_analyzer.cc:146
msgid "Number of subnets:"
msgstr "Antal undernet:"

#: ../src/gui_ipv4_analyzer.cc:149
msgid "Number of hosts:"
msgstr "Antal vÃŠrtsmaskiner:"

#: ../src/gui_ipv4_analyzer.cc:152
msgid "Including net/broadcast:"
msgstr "Inklusiv net/rundsending:"

#: ../src/gui_ipv4_analyzer.cc:155 ../src/gui_ipv4_analyzer.cc:210
#: ../src/gui_ipv4_analyzer.cc:260
msgid "Network address:"
msgstr "NetvÃŠrksadresse:"

#: ../src/gui_ipv4_analyzer.cc:158 ../src/gui_ipv4_analyzer.cc:213
#: ../src/gui_ipv4_analyzer.cc:263
msgid "Broadcast address:"
msgstr "Rundsendingsadresse:"

#: ../src/gui_ipv4_analyzer.cc:194
msgid "Binary Output"
msgstr "BinÃŠr uddata"

#: ../src/gui_ipv4_analyzer.cc:244
msgid "Hexadecimal Output"
msgstr "Sekstentalssystems-uddata"

#: ../src/gui_ipv4_subnet_splitter.cc:34
msgid "Subnetted using prefixlength:"
msgstr "Undernet vist ved prÃŠfikslÃŠngde:"

#: ../src/gui_ipv4_subnet_splitter.cc:35
msgid "Showing a maximum of 1000 subnets."
msgstr "Viser maksimalt 1.000 undernet."

#: ../src/gui_mainwindow.cc:39
msgid "IPv4 Address Analyzer"
msgstr "Redskab til analyse af IPv4-adresser"

#: ../src/gui_mainwindow.cc:41
msgid "IPv4 Range to Prefix Converter"
msgstr "IPv4-interval for prÃŠfiksomformer"

#: ../src/gui_mainwindow.cc:43
msgid "IPv4 Subnet Calculator"
msgstr "IPv4-undernetberegner"

#: ../src/gui_mainwindow.cc:49
msgid "Internet Protocol Calculator"
msgstr "Internetprotokolberegner"

#: ../src/gui_mainwindow.cc:95
msgid "_File"
msgstr "_Fil"

#: ../src/gui_mainwindow.cc:101
msgid "_Edit"
msgstr "R_edigÃ©r"

#: ../src/gui_mainwindow.cc:113
msgid "_Help"
msgstr "_HjÃŠlp"

#: ../src/gui_mainwindow.cc:114
msgid "_About"
msgstr "_Om"

#: ../src/gui_prefixlist.cc:37
msgid "Prefix"
msgstr "PrÃŠfiks"

#: ../src/gui_prefixlist.cc:38
msgid "Subnet Mask"
msgstr "Undernetmaske"
