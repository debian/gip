# translation of gip-1.5.0.pot to Finnish
# Copyright © 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the gip package.
# Jorma Karvonen <karvjorm@users.sf.net>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gip 1.5.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-05-20 20:08+0200\n"
"PO-Revision-Date: 2008-01-08 20:47+0200\n"
"Last-Translator: Jorma Karvonen <karvjorm@users.sf.net>\n"
"Language-Team: Finnish <translation-team-fi@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../src/gui_aboutbox.cc:34
#, c-format
msgid "Gip Release %s %s"
msgstr "Gip-versio %s %s"

#: ../src/gui_aboutbox.cc:45
msgid "Translation of this version by:"
msgstr "Tämän version suomentaja:"

#: ../src/gui_aboutbox.cc:46
msgid "translator_credits"
msgstr "Jorma Karvonen"

#: ../src/gui_aboutbox.cc:52
msgid ""
"Gip, the Internet Protocol Calculator for the GNOME desktop environment was written and published under the terms of the GPL (General Public License V2)\n"
"by Samuel Abels"
msgstr ""
"Gip on Samuels Abelsin Gnome-työpöytäympäristöön kehittämä Internet-yhteyskäytäntölaskuri. Se on julkaistu GPL (General Public License, versio 2)\n"
"-ehtojen mukaisesti"

#: ../src/gui_aboutbox.cc:57
msgid "Thank You for choosing Gip!"
msgstr "Kiitoksia, että valitsit Gip-ohjelman!"

#: ../src/gui_aboutbox.cc:60
msgid "Copyright 2004. All rights reserved."
msgstr "Copyright 2004. Kaikki oikeudet pidätetty."

#: ../src/gui_ipv4_analyzer.cc:53
msgid "Input"
msgstr "Syöte"

#: ../src/gui_ipv4_analyzer.cc:62 ../src/gui_ipv4_analyzer.cc:204
#: ../src/gui_ipv4_analyzer.cc:254
msgid "IP address:"
msgstr "IP-osoite:"

#: ../src/gui_ipv4_analyzer.cc:65 ../src/gui_ipv4_analyzer.cc:207
#: ../src/gui_ipv4_analyzer.cc:257
msgid "Network mask:"
msgstr "Verkon peite:"

#: ../src/gui_ipv4_analyzer.cc:68
msgid "Prefix length:"
msgstr "Prefiksipituus:"

#: ../src/gui_ipv4_analyzer.cc:133
msgid "Output"
msgstr "Tuloste"

#: ../src/gui_ipv4_analyzer.cc:143 ../src/gui_ipv4_subnet_calculator.cc:32
#: ../src/gui_ipv4_subnet_splitter.cc:32
msgid "Address range:"
msgstr "Osoitteiden alue:"

#: ../src/gui_ipv4_analyzer.cc:146
msgid "Number of subnets:"
msgstr "Aliverkkojen lukumäärä:"

#: ../src/gui_ipv4_analyzer.cc:149
msgid "Number of hosts:"
msgstr "Verkkolaitteiden lukumäärä:"

#: ../src/gui_ipv4_analyzer.cc:152
msgid "Including net/broadcast:"
msgstr "Sisältyvät verkko/yleislähetykset:"

#: ../src/gui_ipv4_analyzer.cc:155 ../src/gui_ipv4_analyzer.cc:210
#: ../src/gui_ipv4_analyzer.cc:260
msgid "Network address:"
msgstr "Verkko-osoite:"

#: ../src/gui_ipv4_analyzer.cc:158 ../src/gui_ipv4_analyzer.cc:213
#: ../src/gui_ipv4_analyzer.cc:263
msgid "Broadcast address:"
msgstr "Yleislähetyksen osoite:"

#: ../src/gui_ipv4_analyzer.cc:194
msgid "Binary Output"
msgstr "Binäärituloste"

#: ../src/gui_ipv4_analyzer.cc:244
msgid "Hexadecimal Output"
msgstr "Heksadesimaalituloste"

#: ../src/gui_ipv4_subnet_splitter.cc:34
msgid "Subnetted using prefixlength:"
msgstr "Aliverkotettu käyttäen prefiksipituutta:"

#: ../src/gui_ipv4_subnet_splitter.cc:35
msgid "Showing a maximum of 1000 subnets."
msgstr "Näyttää korkeintaan 1000 aliverkkoa."

#: ../src/gui_mainwindow.cc:39
msgid "IPv4 Address Analyzer"
msgstr "IPv4-osoitteiden analysoija"

#: ../src/gui_mainwindow.cc:41
msgid "IPv4 Range to Prefix Converter"
msgstr "IPv4-alueen muunto prefiksiksi"

#: ../src/gui_mainwindow.cc:43
msgid "IPv4 Subnet Calculator"
msgstr "IPv4-aliverkkolaskuri"

#: ../src/gui_mainwindow.cc:49
msgid "Internet Protocol Calculator"
msgstr "Internet-yhteyskäytäntölaskuri"

#: ../src/gui_mainwindow.cc:95
msgid "_File"
msgstr "_Tiedosto"

#: ../src/gui_mainwindow.cc:101
msgid "_Edit"
msgstr "_Muokkaa"

#: ../src/gui_mainwindow.cc:113
msgid "_Help"
msgstr "O_hje"

#: ../src/gui_mainwindow.cc:114
msgid "_About"
msgstr "_Ohjelmasta"

#: ../src/gui_prefixlist.cc:37
msgid "Prefix"
msgstr "Prefiksi"

#: ../src/gui_prefixlist.cc:38
msgid "Subnet Mask"
msgstr "Aliverkon peite"
