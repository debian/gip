#!/bin/sh
## This script generates a new potfile and re-builds all .mo files.
## Also, POTFILES.in will be re-generated.
##

# Ger a list of all files that are potentially holding translatable strings.
FILES=`find ../src/ -name "*.cc" -o -name "*.c"`

# Re-generate (merge) the potfile.
touch gip.pot
xgettext -jC -k"_" -k"N_" --from-code ISO-8859-1 -o gip.pot $FILES \
 || exit 1

# Generate .mo files from all .po files.
find . -name "*.po" | while read POFILE; do
  MOFILE=`echo $POFILE | sed 's/\.po$/\.mo/'`
  echo -n $POFILE...
  msgfmt -o $MOFILE $POFILE || exit 1
  echo done.
done

# Generate a POTFILE.in file for intltool-update support.
echo GETTEXT_PACKAGE=gip > ../configure.ac || exit 1
touch POTFILES.in                          || exit 1
intltool-update -m                         || exit 1
if [ -e missing ]; then
  mv missing POTFILES.in                   || exit 1
fi

rm ../configure.ac
